package com.cb.model;

import java.util.List;
import java.util.Set;

import org.bson.Document;

public class Contact extends Document{
	private String name;
	private String lastName;
	private Set<String> phone;
	
	public Contact(Document document) {
		this.name = document.getString("name");
		this.lastName = document.getString("lastName");
		this.phone = document.keySet();
	}
	
	public Contact(String name, String lastName, Set<String> phone) {
		super();
		this.name = name;
		this.lastName = lastName;
		this.phone = phone;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public Set<String> getPhone() {
		return phone;
	}
	public void setPhone(Set<String> phone) {
		this.phone = phone;
	}
	
//	@Override
//	public boolean equals(Object obj) {
//		if(obj instanceof Contact){
//			Contact contact = (Contact)obj;
//			if(this.getName() == contact.getName() && this.getLastName() == contact.getLastName()){
//				this.getPhone().add((String)contact.getPhone().toArray()[0]);
//				return true;
//			}else{
//				return false;
//			}
//		}else{
//			return false;
//		}
//	}
	
}
