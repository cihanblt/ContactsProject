package com.cb.main;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.bson.Document;
import org.bson.codecs.Codec;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;

import com.cb.codecs.ContactCodec;
import com.cb.contactprocessing.ContactProcessing;
import com.cb.model.Contact;
import com.cb.xmlprocessing.XMLProcessing;
import com.mongodb.DB;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoDatabase;

/**
 * cihanblt
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	ContactProcessing contactProcessing = new ContactProcessing();
    	List<Contact> contacts = contactProcessing.getContactsList( new File("C:/vngrs/contacts.xml"));
    	List<Contact> mergedContactList = contactProcessing.mergeDuplicateRecords(contacts);
    	
//    	XMLProcessing.writeDataFile(mergedContactList, new File("C:/vngrs/contacts.data"));
    	
    	//for bulk operations
//    	CodecRegistry codecRegistry = CodecRegistries.fromCodecs(new ContactCodec());
//    	Codec<Contact> codec = codecRegistry.get(Contact.class);
//    	MongoClientOptions clientOptions =MongoClientOptions.builder().codecRegistry(codecRegistry).build();
    	
    	
    	MongoClient client = new MongoClient(new ServerAddress("localhost", 27017));
    	MongoDatabase db = client.getDatabase("vngrs_db");
    	List<String> phones = null;
    	for (Contact contact : mergedContactList) {
    		Iterator<String> iterator = contact.getPhone().iterator();
    		phones = new ArrayList<String>();
    		while (iterator.hasNext()) {
				String string = (String) iterator.next();
				phones.add(string);
			}
    		db.getCollection("contacts").insertOne(new Document("contact",
    				new Document("name",contact.getName()).append("lastName", contact.getLastName()).append("phone",phones)
    				));
		}
    	
    	List<Document> foundDocument = db.getCollection("contacts").find().into(new ArrayList<Document>());
    	for (Document document : foundDocument) {
			System.out.println(document.get("contact"));
		}
    	
    	
    }
}
